import inspect
from enolib import messages

def test_message_locales_outside_default_en_locale():
    for key, translation in messages.__dict__.items():
        if key.startswith('__'):
            continue

        if callable(translation):
            parameters = ['PLACEHOLDER'] * len(inspect.signature(translation).parameters)
            result = translation(*parameters)
            assert isinstance(result, str)
        else:
            assert isinstance(translation, str)
