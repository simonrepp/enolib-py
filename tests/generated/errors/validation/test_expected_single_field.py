# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import enolib

def test_expecting_a_field_but_getting_two_fields_raises_the_expected_validationerror():
    error = None
    
    input = ("field: value\n"
             "field: value")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               " >    1 | field: value\n"
               " >    2 | field: value")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 0
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 0
    assert error.selection['to']['column'] == 12

def test_expecting_a_field_but_getting_two_fields_with_comments_and_empty_lines_raises_the_expected_validationerror():
    error = None
    
    input = ("> comment\n"
             "field: value\n"
             "\n"
             "field: value\n"
             "> comment")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               "      1 | > comment\n"
               " >    2 | field: value\n"
               "      3 | \n"
               " >    4 | field: value\n"
               "      5 | > comment")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 1
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 1
    assert error.selection['to']['column'] == 12

def test_expecting_a_field_but_getting_two_fields_raises_the_expected_validationerror():
    error = None
    
    input = ("field:\n"
             "attribute = value\n"
             "field:\n"
             "attribute = value")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               " >    1 | field:\n"
               " *    2 | attribute = value\n"
               " >    3 | field:\n"
               " *    4 | attribute = value")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 0
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 1
    assert error.selection['to']['column'] == 17

def test_expecting_a_field_but_getting_two_fields_with_attributes_comments_and_empty_lines_raises_the_expected_validationerror():
    error = None
    
    input = ("> comment\n"
             "field:\n"
             "attribute = value\n"
             "\n"
             "attribute = value\n"
             "\n"
             "field:\n"
             "> comment\n"
             "attribute = value")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               "      1 | > comment\n"
               " >    2 | field:\n"
               " *    3 | attribute = value\n"
               " *    4 | \n"
               " *    5 | attribute = value\n"
               "      6 | \n"
               " >    7 | field:\n"
               " *    8 | > comment\n"
               " *    9 | attribute = value")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 1
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 4
    assert error.selection['to']['column'] == 17

def test_expecting_a_field_but_getting_two_fields_with_items_raises_the_expected_validationerror():
    error = None
    
    input = ("field:\n"
             "- item\n"
             "field:\n"
             "- item")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               " >    1 | field:\n"
               " *    2 | - item\n"
               " >    3 | field:\n"
               " *    4 | - item")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 0
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 1
    assert error.selection['to']['column'] == 6

def test_expecting_a_field_but_getting_two_fields_with_items_comments_and_empty_lines_raises_the_expected_validationerror():
    error = None
    
    input = ("> comment\n"
             "field:\n"
             "- item\n"
             "\n"
             "- item\n"
             "\n"
             "field:\n"
             "> comment\n"
             "- item")
    
    try:
        enolib.parse(input).field('field')
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("Only a single field with the key 'field' was expected.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               "      1 | > comment\n"
               " >    2 | field:\n"
               " *    3 | - item\n"
               " *    4 | \n"
               " *    5 | - item\n"
               "      6 | \n"
               " >    7 | field:\n"
               " *    8 | > comment\n"
               " *    9 | - item")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 1
    assert error.selection['from']['column'] == 0
    assert error.selection['to']['line'] == 4
    assert error.selection['to']['column'] == 6