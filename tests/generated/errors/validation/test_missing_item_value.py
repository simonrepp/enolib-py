# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import enolib

def test_directly_querying_an_item_for_a_required_but_missing_value_raises_the_expected_validationerror():
    error = None
    
    input = ("field:\n"
             "-")
    
    try:
        enolib.parse(input).field('field').items()[0].required_value()
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("The field 'field' may not contain empty items.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               "      1 | field:\n"
               " >    2 | -")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 1
    assert error.selection['from']['column'] == 1
    assert error.selection['to']['line'] == 1
    assert error.selection['to']['column'] == 1

def test_indirectly_querying_a_field_with_empty_items_for_required_values_raises_the_expected_validationerror():
    error = None
    
    input = ("field:\n"
             "-")
    
    try:
        enolib.parse(input).field('field').required_values()
    except enolib.ValidationError as _error:
        if isinstance(_error, enolib.ValidationError):
            error = _error
        else:
            raise _error

    assert type(error) is enolib.ValidationError
    
    text = ("The field 'field' may not contain empty items.")
    
    assert error.text == text
    
    snippet = ("   Line | Content\n"
               "      1 | field:\n"
               " >    2 | -")
    
    assert error.snippet == snippet
    
    assert error.selection['from']['line'] == 1
    assert error.selection['from']['column'] == 1
    assert error.selection['to']['line'] == 1
    assert error.selection['to']['column'] == 1