# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

import enolib

def test_querying_an_existing_required_value_from_an_embed_produces_the_expected_result():
    input = ("-- embed\n"
             "value\n"
             "-- embed")
    
    output = enolib.parse(input).embed('embed').required_value()
    
    expected = ("value")
    
    assert output == expected

def test_querying_an_existing_optional_value_from_an_embed_produces_the_expected_result():
    input = ("-- embed\n"
             "value\n"
             "-- embed")
    
    output = enolib.parse(input).embed('embed').optional_value()
    
    expected = ("value")
    
    assert output == expected

def test_querying_a_missing_optional_value_from_an_embed_produces_the_expected_result():
    input = ("-- embed\n"
             "-- embed")
    
    output = enolib.parse(input).embed('embed').optional_value()
    
    assert output == None