import enolib
from tests.util import snapshot

input = """
> Comment

Field: Value

Field:
- Item
- Item

Field:
Attribute = Value
Attribute = Value

-- Embed
Multiline-
Value
-- Embed

Flag

# Section
## Subsection
### etc.

`Escaped`: Value

`` `Doubly-Escaped` ``: Value

Field:
`Escaped` = Value

> Add additional dashes to escape embed content
--- Embed
-- Embed
--- Embed

`Escaped`
""".strip()
        
def unpack_value(element, target):
    value = element.optional_value()
    if value:
        target['value'] = value

def unpack_section(section):
    section_unpacked = {
        'elements': [],
        'type': 'document' if section.is_document() else 'section'
    }
    
    for element in section.elements():
        if element.is_section():
            section_unpacked['elements'].append(unpack_section(element))
        else:
            unpacked_element = { 'key': element.key() }
            
            if element.is_embed():
                unpacked_element['type'] = 'embed'
                unpack_value(element, unpacked_element)
            elif element.is_field():
                unpacked_element['type'] = 'field'
                if element.has_attributes():
                    unpacked_element['attributes'] = []
                    for attribute in element.attributes():
                        unpacked_attribute = { 'key': attribute.key(), 'type': 'attribute' }
                        unpack_value(attribute, unpacked_attribute)
                        unpacked_element['attributes'].append(unpacked_attribute)
                elif element.has_items():
                    unpacked_element['items'] = []
                    for item in element.items():
                        unpacked_item = { 'type': 'item' }
                        unpack_value(item, unpacked_item)
                        unpacked_element['items'].append(unpacked_item)    
                else:
                    unpack_value(element, unpacked_element)
            elif element.is_flag():
                unpacked_element['type'] = 'flag'
            
            section_unpacked['elements'].append(unpacked_element)
        
    return section_unpacked

def test_blackbox_test_cheatsheet():
    document = enolib.parse(input)
    document_unpacked = unpack_section(document)

    assert document_unpacked == snapshot(document_unpacked, 'tests/blackbox/snapshots/cheatsheet.snap.json')
