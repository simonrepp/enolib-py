from . import messages
from .analyzer import Analyzer
from .reporters.text_reporter import TextReporter
from .constants import (
    BEGIN,
    DOCUMENT,
    EMBED_BEGIN,
    END
)

class Context:
    def __init__(self, input: str, *, locale=messages, reporter=TextReporter, source=None):
        self.input = input
        self.messages = locale
        self.reporter = reporter
        self.source = source

        self.document = {
            'elements': [],
            'line': 0,
            'ranges': { 'line': (0, 0) },
            'type': DOCUMENT
        }

        self.meta = []

        Analyzer(self).analyze()

    def value(self, element):
        if 'computed_value' not in element:
            element['computed_value'] = None

            if element['type'] is EMBED_BEGIN:
                if 'lines' in element:
                    element['computed_value'] = self.input[
                        element['lines'][0]['ranges']['line'][BEGIN]:element['lines'][-1]['ranges']['line'][END]
                    ]
            else:
                if 'value' in element:
                    element['computed_value'] = element['value']

        return element['computed_value']
