# Added to 0-indexed indices in a few places
HUMAN_INDEXING = 1

# Selection indices
BEGIN = 0
END = 1

# Instruction types
ATTRIBUTE = 'Attribute'
COMMENT = 'Comment'
DOCUMENT = 'Document'
EMBED_BEGIN = 'Embed Begin'
EMBED_END = 'Embed End'
EMBED_VALUE = 'Embed Value'
FIELD = 'Field'
FLAG = 'Flag'
ITEM = 'Item'
SECTION = 'Section'
UNPARSED = 'Unparsed'
