from .missing_element_base import MissingElementBase

class MissingValueElementBase(MissingElementBase):
    def optional_value(self, _loader=None):
        return None

    def required_value(self, _loader=None):
        self._parent._missing_error(self)
