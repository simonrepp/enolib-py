class MissingElementBase:
    def __init__(self, key, parent):
        self._key = key
        self._parent = parent

    def _missing_error(self, _element):
        self._parent._missing_error(self)

    def key(self, _loader=None):
        self._parent._missing_error(self)