from ..errors.validation import Validation
from ..constants import ATTRIBUTE, DOCUMENT, EMBED_BEGIN, FIELD, FLAG, ITEM, SECTION

class ElementBase:
    def __init__(self, context, instruction, parent=None):
        self._context = context
        self._instruction = instruction
        self._parent = parent  # TODO: Could be set optionally as well (needs hasattr checks everywhere then)

    def _key(self):
        if self._instruction['type'] is DOCUMENT:
            return None

        if self._instruction['type'] is ITEM:
            return self._instruction['parent']['key']

        return self._instruction['key']

    def _untouched(self):
        if not hasattr(self, '_touched'):
            return self._instruction

    def error(self, message):
        if callable(message):
            message = message(self)  # Revisit self in this context - problematic

        return Validation.element_error(self._context, message, self._instruction)

    def has_attributes(self):
        return 'attributes' in self._instruction

    def has_items(self):
        return 'items' in self._instruction

    def has_value(self):
        return 'value' in self._instruction
        
    def is_attribute(self):
        return self._instruction['type'] == ATTRIBUTE

    def is_document(self):
        return self._instruction['type'] == DOCUMENT
        
    def is_embed(self):
        return self._instruction['type'] == EMBED_BEGIN
        
    def is_field(self):
        return self._instruction['type'] == FIELD
        
    def is_flag(self):
        return self._instruction['type'] == FLAG
        
    def is_item(self):
        return self._instruction['type'] == ITEM
        
    def is_section(self):
        return self._instruction['type'] == SECTION

    def key(self, loader=None):
        self._touched = True

        if not loader:
            return self._key()

        try:
            return loader(self._key())
        except ValueError as message:
            raise Validation.key_error(self._context, message, self._instruction)

    def key_error(self, message):
        if callable(message):
            message = message(self._key())

        return Validation.key_error(self._context, message, self._instruction)

    def touch(self):
        self._touched = True
