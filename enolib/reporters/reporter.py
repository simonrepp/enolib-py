from ..constants import (
    DOCUMENT,
    EMBED_BEGIN,
    FIELD,
    SECTION
)

DISPLAY = 'Display Line'
EMPHASIZE = 'Emphasize Line'
INDICATE = 'Indicate Line'
OMISSION = 'Insert Omission'
QUESTION = 'Question Line'

class Reporter:
    def __init__(self, context):
        self._context = context
        self._index = [None] * self._context.line_count
        self._snippet = [None] * self._context.line_count

        self._build_index()

    def _build_index(self):
        # TODO: Put this into the root class scope?
        def traverse(section):
            for element in section['elements']:
                self._index[element['line']] = element

                if element['type'] == SECTION:
                    traverse(element)
                elif element['type'] == FIELD:
                    if 'attributes' in element:
                        for attribute in element['attributes']:
                            self._index[attribute['line']] = attribute
                    elif 'items' in element:
                        for item in element['items']:
                            self._index[item['line']] = item
                elif element['type'] == EMBED_BEGIN:
                    # Missing when reporting an unterminated embed
                    if 'end' in element:
                        self._index[element['end']['line']] = element['end']

                    if 'lines' in element:
                        for line in element['lines']:
                            self._index[line['line']] = line

        traverse(self._context.document)

        for meta in self._context.meta:
            self._index[meta['line']] = meta

    def _tag_attributes_or_items(self, element, collection, tag):
        scan_line = element['line'] + 1

        if not collection in element:
            return scan_line

        for attribute_or_item in element[collection]:
            while scan_line < attribute_or_item['line']:
                self._snippet[scan_line] = tag
                scan_line += 1

            self._snippet[attribute_or_item['line']] = tag

            scan_line = attribute_or_item['line'] + 1

        return scan_line

    def _tag_children(self, element, tag):
        if element['type'] == FIELD:
            if 'attributes' in element:
                return self._tag_attributes_or_items(element, 'attributes', tag)
            elif 'items' in element:
                return self._tag_attributes_or_items(element, 'items', tag)
            else:
                return element['line'] + 1
        elif element['type'] == EMBED_BEGIN:
            if 'lines' in element:
                for line in element['lines']:
                    self._snippet[line['line']] = tag

            if 'end' in element:
                self._snippet[element['end']['line']] = tag
                return element['end']['line'] + 1
            elif 'lines' in element:
                return element['lines'][-1]['line'] + 1
            else:
                return element['line'] + 1
        elif element['type'] == SECTION:
            return self._tag_section(element, tag)

    def _tag_section(self, section, tag, recursive=True):
        scan_line = section['line'] + 1

        for element in section['elements']:
            while scan_line < element['line']:
                self._snippet[scan_line] = tag
                scan_line += 1

            if not recursive and element['type'] == SECTION:
                break

            self._snippet[element['line']] = tag

            scan_line = self._tag_children(element, tag)

        return scan_line
        
    def indicate_element(self, element):
        self._snippet[element['line']] = INDICATE
        self._tag_children(element, INDICATE)

        return self

    def indicate_line(self, element):
        self._snippet[element['line']] = INDICATE
        return self

    def question_line(self, element):
        self._snippet[element['line']] = QUESTION
        return self

    def report_element(self, element):
        self._snippet[element['line']] = EMPHASIZE
        self._tag_children(element, INDICATE)

        return self

    def report_elements(self, elements):
        for element in elements:
            self._snippet[element['line']] = EMPHASIZE
            self._tag_children(element, INDICATE)

        return self

    def report_line(self, instruction):
        self._snippet[instruction['line']] = EMPHASIZE

        return self

    def report_multiline_value(self, element):
        for line in element['lines']:
            self._snippet[line['line']] = EMPHASIZE

        return self

    def report_missing_element(self, parent):
        if parent['type'] != DOCUMENT:
            self._snippet[parent['line']] = INDICATE

        if parent['type'] == SECTION:
            self._tag_section(parent, QUESTION, False)
        else:
            self._tag_children(parent, QUESTION)

        return self

    def snippet(self):
        if all(tag is None for tag in self._snippet):
            for line in range(len(self._snippet)):
                self._snippet[line] = QUESTION
        else:
            for line, tag in enumerate(self._snippet):
                if tag is not None:
                    continue

                if(line + 2 < self._context.line_count and self._snippet[line + 2] is not None and self._snippet[line + 2] != DISPLAY or
                   line - 2 >= 0 and self._snippet[line - 2] is not None and self._snippet[line - 2] != DISPLAY or
                   line + 1 < self._context.line_count and self._snippet[line + 1] is not None and self._snippet[line + 1] != DISPLAY or
                   line - 1 >= 0 and self._snippet[line - 1] is not None and self._snippet[line - 1] != DISPLAY):
                    self._snippet[line] = DISPLAY
                elif line + 3 < self._context.line_count and self._snippet[line + 3] is not None and self._snippet[line + 3] != DISPLAY:
                    self._snippet[line] = OMISSION

            if self._snippet[-1] is None:
                self._snippet[-1] = OMISSION

        return self._print()
