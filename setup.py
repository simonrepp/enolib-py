from setuptools import find_packages, setup

with open('README.md', 'r') as file:
    long_description = file.read()

setup(author='Simon Repp',
      classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries"
      ],
      description='An eno parsing library',
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT',
      name='enolib',
      packages=find_packages(exclude=['performance', 'performance.*', 'tests', 'tests.*']),
      url='https://eno-lang.org/python/',
      version='0.8.1',
      zip_safe=False)
